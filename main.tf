terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region     = var.aws_default_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

data "template_file" "jenkins_config" {
  template = file("jenkins.yaml")

  vars = {
    GITLAB_API_TOKEN       = "${var.gitlab_api_token}"
    JENKINS_ADMIN_ID       = "${var.jenkins_admin_id}"
    JENKINS_ADMIN_PASSWORD = "${var.jenkins_admin_password}"
    AWS_DEFAULT_REGION     = "${var.aws_default_region}"
    AWS_SECRET_KEY         = "${var.aws_secret_key}"
    AWS_ACCESS_KEY         = "${var.aws_access_key}"
  }
}

data "template_file" "jenkins_setup_script" {
  template = file("jenkins_setup.sh")

  vars = {
    DOCKERFILE             = file("Dockerfile")
    JENKINS_CONFIG         = data.template_file.jenkins_config.rendered
    JENKINS_PLUGINS        = file("jenkins_plugins.txt")
    JENKINS_ADMIN_ID       = "${var.jenkins_admin_id}"
    JENKINS_ADMIN_PASSWORD = "${var.jenkins_admin_password}"
  }
}

data "template_cloudinit_config" "jenkins_server_config" {
  gzip          = true
  base64_encode = true

  # Main cloud-config configuration file.
  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.jenkins_setup_script.rendered
  }
}

module "aws_s3_bucket" {
  source = "./modules/aws-s3"

  bucket_name = "tf-devops-ui"
}

module "aws_ec2" {
  source = "./modules/aws-ec2"

  aws_instance_key        = "jenkins-server"
  aws_security_group_name = "jenkins-server-security-group"
  aws_instance_type       = "t3.medium"
  user_data               = data.template_cloudinit_config.jenkins_server_config.rendered
}
