variable "aws_secret_key" {}
variable "aws_access_key" {}
variable "aws_default_region" {
  default = "us-east-1"
}

variable "jenkins_admin_id" {}
variable "jenkins_admin_password" {}

variable "gitlab_api_token" {}
