#!/usr/bin/env groovy

pipeline {
  agent any

  tools {
    nodejs "node-18"
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3'))
  }

  stages {
    stage('Fetch') {
      steps {
        updateGitlabCommitStatus name: 'fetch', state: 'running'
        sh 'ls -A1 | xargs rm -rf'
        sh 'git clone https://gitlab.com/iv-course/devops-ui.git .'
      }

      post {
        failure {
          updateGitlabCommitStatus name: 'fetch', state: 'failed'
        }
        success {
          updateGitlabCommitStatus name: 'fetch', state: 'success'
        }
        aborted {
          updateGitlabCommitStatus name: 'fetch', state: 'canceled'
        }
      }
    }

    stage('Install') {
      steps {
        updateGitlabCommitStatus name: 'install', state: 'running'
        sh 'npm install'
      }

      post {
        failure {
          updateGitlabCommitStatus name: 'install', state: 'failed'
        }
        success {
          updateGitlabCommitStatus name: 'install', state: 'success'
        }
        aborted {
          updateGitlabCommitStatus name: 'install', state: 'canceled'
        }
      }
    }

    stage('Test') {
      steps {
        updateGitlabCommitStatus name: 'test', state: 'running'
        sh 'npm run test'
      }

      post {
        failure {
          updateGitlabCommitStatus name: 'test', state: 'failed'
        }
        success {
          updateGitlabCommitStatus name: 'test', state: 'success'
        }
        aborted {
          updateGitlabCommitStatus name: 'test', state: 'canceled'
        }
      }
    }

    stage('Build') {
      steps {
        updateGitlabCommitStatus name: 'build', state: 'running'
        sh 'npm run build'
      }

      post {
        failure {
          updateGitlabCommitStatus name: 'build', state: 'failed'
        }
        success {
          updateGitlabCommitStatus name: 'build', state: 'success'
        }
        aborted {
          updateGitlabCommitStatus name: 'build', state: 'canceled'
        }
      }
    }

    stage('Deploy') {
      input {
        message "Proceed with deployment?"
        ok "Yes"
      }

      steps {
        updateGitlabCommitStatus name: 'deploy', state: 'running'
        sh "aws configure set region $AWS_DEFAULT_REGION" 
        sh "aws configure set aws_access_key_id $AWS_ACCESS_KEY"  
        sh "aws configure set aws_secret_access_key $AWS_SECRET_KEY"
        sh "aws s3 rm s3://tf-devops-ui --recursive"
        sh "aws s3 cp dist/devops-ui/ s3://tf-devops-ui --recursive"
      }

      post {
        failure {
          updateGitlabCommitStatus name: 'deploy', state: 'failed'
        }
        success {
          updateGitlabCommitStatus name: 'deploy', state: 'success'
        }
        aborted {
          updateGitlabCommitStatus name: 'deploy', state: 'canceled'
        }
      }
    }
  }
}
