output "eip" {
  description = "Elastic IP address for EC2"
  value       = aws_eip.eip.*.public_ip
}

output "public_dns" {
  description = "Public DNS for EC2"
  value       = aws_instance.ec2.public_dns
}
