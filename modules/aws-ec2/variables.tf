variable "user_data" {
  description = "Script that will be executed once EC2 instance is set up."
  type        = string
}

variable "aws_instance_key" {
  description = "Name of EC2 instance."
  type        = string
}

variable "aws_security_group_name" {
  description = "Name of the security group that will be created."
  type        = string
}

variable "ami" {
  description = "AMI to use."
  type        = string
  default     = "ami-0574da719dca65348" # Ubuntu 22.04
}

variable "aws_instance_type" {
  description = "Instance type. Default is t2.micro."
  type        = string
  default     = "t2.micro"
}
