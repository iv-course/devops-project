resource "aws_security_group" "security_group" {
  name        = var.aws_security_group_name
  description = "Allow inbound HTTP and SSH connections"

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "TCP (custom)"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "ec2" {
  ami           = var.ami
  instance_type = var.aws_instance_type
  key_name      = var.aws_instance_key

  security_groups  = [aws_security_group.security_group.name]
  user_data_base64 = var.user_data
}

resource "aws_eip" "eip" {
  instance = aws_instance.ec2.id
}
