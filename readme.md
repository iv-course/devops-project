# Terraform + AWS EC2 + Jenkins

## Table of contents

- [Usage](#usage)
  - [Init](#init)
  - [Update modules](#update-modules)
  - [Plan](#plan)
  - [Apply](#apply)
  - [Destroy](#destroy)
- [Docker](#docker)
- [Debugging](#debugging)
  - [User data](#user-data)
  - [Connect to EC2 via SSH](#connect-to-ec2-via-ssh)

## Usage

### Init

```
terraform init
```

### Update modules

```
terraform get
```

### Plan

```
terraform plan -var-file="variables.tfvars" -out out.tfplan
```

### Apply

```
terraform apply out.tfplan
```

If you do not specify a plan file, add `-var-file="variables.tfvars"`.

### Destroy

```
terraform destroy -var-file="variables.tfvars"
```

## Docker

Show running containers:

```
docker ps
```

Show logs:

```
docker logs -f <Container_ID>
```

Remove container:

```
docker stop <Container_ID>
docker rm <Container_ID>
```

## Debugging

You can set TF_LOG to one of the log levels (in order of decreasing verbosity) TRACE, DEBUG, INFO, WARN or ERROR to change the verbosity of the logs.

Run before any terraform command:

```
export TF_LOG="TRACE"
```

### User data

#### Cloud init

Connect to EC2 instance via SSH.

Execute user data again:

```
sudo  rm -rf /var/lib/cloud/*
sudo cloud-init init
sudo cloud-init modules -m final
```

Show logs:

```
sudo cat /var/log/cloud-init-output.log
```

### Connect to EC2 via SSH

```
ssh -i jenkins-server.pem ubuntu@<public_dns> -o IdentitiesOnly=yes
```

Use `-v` option to show logs.