#!/bin/bash

# Install Docker

sudo apt update
yes | sudo apt install apt-transport-https ca-certificates curl software-properties-common
sudo rm -f /usr/share/keyrings/docker-archive-keyring.gpg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
apt-cache policy docker-ce
yes | sudo apt install docker-ce

# Install JRE

yes | sudo apt install default-jre

# Set up Jenkins

mkdir -p "$HOME"/jenkins
cd "$HOME"/jenkins

touch plugins.txt
echo "${JENKINS_PLUGINS}" >> plugins.txt

touch Dockerfile
echo "${DOCKERFILE}" >> Dockerfile

touch jenkins.yaml
echo "${JENKINS_CONFIG}" >> jenkins.yaml

sudo docker build -t jenkins:jcasc .
sudo docker run --name jenkins --rm \
    -d -p 8080:8080 \
    --env JENKINS_ADMIN_ID="${JENKINS_ADMIN_ID}" \
    --env JENKINS_ADMIN_PASSWORD="${JENKINS_ADMIN_PASSWORD}" \
    jenkins:jcasc