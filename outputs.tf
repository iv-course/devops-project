output "website_bucket_name" {
  description = "Name (id) of the bucket"
  value       = module.aws_s3_bucket.name
}

output "website_bucket_domain" {
  description = "Domain name of the bucket"
  value       = module.aws_s3_bucket.domain
}

output "eip" {
  description = "Elastic IP address for EC2"
  value       = module.aws_ec2.eip
}

output "public_dns" {
  description = "Public DNS for EC2"
  value       = module.aws_ec2.public_dns
}
